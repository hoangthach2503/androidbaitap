package com.example.hoangthach2503.baifirebase.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hoangthach2503.baifirebase.MainActivity;
import com.example.hoangthach2503.baifirebase.R;
import com.example.hoangthach2503.baifirebase.model.JournalEntry;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;


/**
 * Created by Admin on 8/10/2017.
 */

public class JournalAdapter extends ArrayAdapter<JournalEntry> {
    MainActivity context;
    int resource;
    List<JournalEntry> listJournalEntries;

    public JournalAdapter(MainActivity context, int resource, List<JournalEntry> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.listJournalEntries = objects;
    }


    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;
        if ((convertView == null)) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            viewHolder.txtJournalDate = (TextView) convertView.findViewById(R.id.txtJournalDate);
            viewHolder.txtJournalIcon = (TextView) convertView.findViewById(R.id.txtJournalIcon);
            viewHolder.imgDelete = (ImageView) convertView.findViewById(R.id.imgDelete);
            viewHolder.lnContent = (LinearLayout) convertView.findViewById(R.id.lnContent);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final JournalEntry journalEntry = listJournalEntries.get(position);
        viewHolder.txtTitle.setText(journalEntry.getTitle());
        SimpleDateFormat dinhDangDate = new SimpleDateFormat("dd/MM/yyyy ");
        String strDateModified = dinhDangDate.format(journalEntry.getDateModified());
        viewHolder.txtJournalDate.setText(strDateModified);
        viewHolder.txtJournalIcon.setText(journalEntry.getTitle().substring(0, 1).toUpperCase());
        int[] androidColors = context.getResources().getIntArray(R.array.androidcolors);
        int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
        viewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 context.DialogDelete(journalEntry.getJournalId() );

            }
        });

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setCornerRadius(100f);
        gradientDrawable.setColor(randomAndroidColor);
        viewHolder.txtJournalIcon.setBackground(gradientDrawable);
        return convertView;
    }


    private class ViewHolder {
        public TextView txtTitle;
        public TextView txtJournalDate;
        public TextView txtJournalIcon;
        public ImageView imgDelete;
        public LinearLayout lnContent;
    }
}
