package com.example.hoangthach2503.baifirebase.controller;

import com.example.hoangthach2503.baifirebase.model.JournalEntry;
import com.example.hoangthach2503.baifirebase.model.Tag;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Admin on 8/10/2017.
 */

public class SampleData {
    public static List<JournalEntry> getSampleJournalEntries() {

        List<JournalEntry> journalEnrties = new ArrayList<>();
        //create the dummy journal
        JournalEntry journalEntry1 = new JournalEntry();
        journalEntry1.setTitle("Notes from Networking Event");
        journalEntry1.setContent
                ("We went to Disneyland today and the kids had lots of fun!");
        Calendar calendar1 = GregorianCalendar.getInstance();
        journalEntry1.setDateModified(calendar1.getTimeInMillis());
        journalEnrties.add(journalEntry1);
        return journalEnrties;
    }
    public static List<String> getSampleTags(){
        List<String> tagnames = new ArrayList<>();
        //create the dummy journal
        Tag tag1 = new Tag();
        tag1.setTagName("Name");
        tagnames.add(tag1.getTagName());
        return tagnames;
    }
}
