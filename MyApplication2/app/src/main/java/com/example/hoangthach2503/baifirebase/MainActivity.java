package com.example.hoangthach2503.baifirebase;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hoangthach2503.baifirebase.adapter.JournalAdapter;
import com.example.hoangthach2503.baifirebase.controller.SampleData;
import com.example.hoangthach2503.baifirebase.model.JournalEntry;
import com.example.hoangthach2503.baifirebase.model.Tag;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.category;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private DatabaseReference journalCloudEndPoint;
    private DatabaseReference tagCloudEndPoint;
    private JournalAdapter mAdapter;
    private List<JournalEntry> mJournalEntries;
    private List<Tag> mTags=new ArrayList<Tag>();
    private List<String> tagNames = SampleData.getSampleTags();
    private ListView lvJournalEntries;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mJournalEntries = new ArrayList<JournalEntry>();
        lvJournalEntries = (ListView) findViewById(R.id.lvJournalEntries);
        mAdapter = new JournalAdapter(this, R.layout.dong_journal_entry, mJournalEntries);
        lvJournalEntries.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        journalCloudEndPoint = mDatabase.child("journalentris");
        tagCloudEndPoint = mDatabase.child("tags");
        addInitialDataToFirebase();
        getInitialDataBase();
    }
    public void DialogDelete(final String keyId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Bạn chắc chắn muốn xóa?");
        builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                journalCloudEndPoint.child( keyId).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        if(mAdapter.getCount() < 1){
                            Toast.makeText(MainActivity.this, "Danh Sách Trống", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                int dem=0;
                for (JournalEntry j : mJournalEntries) {
                    if (j.getJournalId() == keyId) {
                        mJournalEntries.remove(j);
                        mAdapter.notifyDataSetChanged();
                        tagCloudEndPoint.child(mTags.get(dem).getTagId()).removeValue();
                        break;
                    }
                    dem++;
                }
            }
        });
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    public void getInitialDataBase() {
        journalCloudEndPoint.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                JournalEntry note = dataSnapshot.getValue(JournalEntry.class);
                mJournalEntries.add(note);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                mJournalEntries.clear();
                JournalEntry note = dataSnapshot.getValue(JournalEntry.class);
                mJournalEntries.add(note);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void addInitialDataToFirebase() {

        List<JournalEntry> sampleJournalEntries = SampleData.getSampleJournalEntries();
        for (JournalEntry journalEntry : sampleJournalEntries) {
            String key = journalCloudEndPoint.push().getKey();
            journalEntry.setJournalId(key);
            journalCloudEndPoint.child(key).setValue(journalEntry);
        }

        for (String name : tagNames) {
            String tagKey = tagCloudEndPoint.push().getKey();
            Tag tag = new Tag();
            tag.setTagName(name);
            tag.setTagId(tagKey);
            mTags.add(tag);
            tagCloudEndPoint.child(tag.getTagId()).setValue(category);
        }

    }
}
