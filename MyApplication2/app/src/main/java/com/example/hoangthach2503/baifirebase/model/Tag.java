package com.example.hoangthach2503.baifirebase.model;

/**
 * Created by Admin on 8/10/2017.
 */

public class Tag {
    private String tagId;
    private String tagName;
    private int journalCount;

    public Tag(String tagId, String tagName, int journalCount) {
        this.tagId = tagId;
        this.tagName = tagName;
        this.journalCount = journalCount;
    }

    public Tag() {
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public int getJournalCount() {
        return journalCount;
    }

    public void setJournalCount(int journalCount) {
        this.journalCount = journalCount;
    }

    @Override
    public String toString() {
        return "Tag{" +
                "tagId='" + tagId + '\'' +
                ", tagName='" + tagName + '\'' +
                ", journalCount=" + journalCount +
                '}';
    }
}
