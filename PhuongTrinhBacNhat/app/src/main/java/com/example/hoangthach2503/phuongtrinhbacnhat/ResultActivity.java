package com.example.hoangthach2503.phuongtrinhbacnhat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView txtKq;
    EditText edtA, edtB;
    Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        btnBack = (Button) findViewById(R.id.btnBack);
        txtKq = (TextView) findViewById(R.id.txtKq);
        edtA = (EditText) findViewById(R.id.editTextA);
        edtB = (EditText) findViewById(R.id.editTextB);
        Intent callerIntent = getIntent();
        Bundle packBundle = callerIntent.getBundleExtra("MyPackage");
        double a = packBundle.getDouble("soA");
        double b = packBundle.getDouble("soB");
        giaiPtBacNhat(a, b);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private void giaiPtBacNhat(double a, double b) {
        if (a == 0) {
            if (b == 0)
                txtKq.setText("Phương trình có vô số nghiệm");
            else txtKq.setText("Phương trình vô nghiệm");
        } else txtKq.setText("Kết quả: " + (-b / a));

    }


}
