﻿package com.example.hoangthach2503.phuongtrinhbacnhat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {
EditText edtA, edtB;
    Button btnKetQua;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnKetQua= (Button) findViewById(R.id.btnKetQua);
        edtA=(EditText) findViewById(R.id.editTextA);
        edtB=(EditText) findViewById(R.id.editTextB);
        btnKetQua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtA.getText().toString().isEmpty()||edtB.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Bạn nhập đầy đủ", Toast.LENGTH_SHORT).show();
                }
                Intent intent= new Intent(MainActivity.this, ResultActivity.class);
                double a=Double.parseDouble(edtA.getText().toString());
               double b=Double.parseDouble(edtB.getText().toString());

                Bundle bundle=new Bundle();
                bundle.putDouble("soA", a);
                bundle.putDouble("soB", b);
                intent.putExtra("MyPackage",bundle);
                startActivityForResult(intent, 123);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==123&&resultCode==RESULT_OK){
            Toast.makeText(this, " Wellcome back to MainActivity ! Your last edit text : a= ...  ,b= ...   ", Toast.LENGTH_SHORT).show();
            edtA.setText("0");
            edtB.setText("0");
        }
//
        super.onActivityResult(requestCode, resultCode, data);
    }
}
