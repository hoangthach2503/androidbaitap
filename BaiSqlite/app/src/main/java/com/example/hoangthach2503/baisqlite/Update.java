package com.example.hoangthach2503.baisqlite;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Update extends AppCompatActivity {
    private EditText edtName, edtNumber, edtAddress;
    private MyDataBase dataBase;
    private String gendle;
    private RadioButton rdMale;
    private RadioButton rdFemale;
    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        init();
        Intent intent = getIntent();
        contact = (Contact) intent.getSerializableExtra(ContactAdapter.KEYUPDATE);
        edtAddress.setText(contact.getAddress());
        edtName.setText(contact.getName());
        edtNumber.setText(contact.getNumber());
        if (contact.getGendle() == "Male")
            rdMale.isChecked();
        else rdFemale.isChecked();
        dataBase = new MyDataBase(this, "contacts.sqlite", null, 1);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCancle:
                finish();
                break;
            case R.id.btnUpdate:
                Update();
                finish();
                MainActivity.getDataContacts();
                break;
        }
    }

    private void Update() {

        String name = edtName.getText().toString();
        String number = edtNumber.getText().toString();
        String address = edtAddress.getText().toString();
        if (name == "" || number == "" || address == "") {
            Toast.makeText(this, "Bạn phải nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
        } else {
            Date time = new Date();
            SimpleDateFormat dinhDangTime = new SimpleDateFormat("HH:mm");
            String showTime = dinhDangTime.format(time.getTime());
            //Date date = new Date();
            SimpleDateFormat dinhDangDate = new SimpleDateFormat("dd/MM/yyyy ");
            String showDate = dinhDangDate.format(time.getTime());
            if (rdFemale.isChecked()) {
                gendle = "Female";
            } else {
                gendle = "Male";
            }
            dataBase.QueryData("UPDATE Contact SET name='" + name + "',address='" + address
                    + "',number ='" + number + "',date= '" + showDate + "', time='" + showTime + "', " +
                    "gendle='" + gendle + "'WHERE id='" + contact.getId() + "'");
            Toast.makeText(this, "Update successful", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_update, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                addDataBase();
                break;
            case R.id.home:
                Intent intent = new Intent(Update.this, MainActivity.class);
               startActivity(intent);
                break;
            case R.id.deleteContact:
                diaglogDelete(contact.getName(), contact.getId());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void diaglogDelete(final String name, final int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to delete contact  " + name + "?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dataBase.QueryData("DELETE FROM Contact WHERE id='" + id + "'");
                Toast.makeText(Update.this, "Deleted contact " + name, Toast.LENGTH_SHORT).show();
                MainActivity.getDataContacts();
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }

    private void addDataBase() {
        Intent mIntent = new Intent(Update.this, Add.class);
        startActivity(mIntent);
    }

    private void init() {
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtName = (EditText) findViewById(R.id.edtName);
        edtNumber = (EditText) findViewById(R.id.edtNumber);
        rdFemale = (RadioButton) findViewById(R.id.rdFemale);
        rdMale = (RadioButton) findViewById(R.id.rdMale);

    }
}
