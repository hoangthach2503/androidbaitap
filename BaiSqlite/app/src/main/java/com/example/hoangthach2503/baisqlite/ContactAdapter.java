package com.example.hoangthach2503.baisqlite;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Admin on 22/10/2017.
 */


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    private List<Contact> contacts;
    private MainActivity context;
    public static String KEYUPDATE = "UPDATE";
    public static String KEYDETAIL = "DETAIL";

    public ContactAdapter(List<Contact> contacts, MainActivity context) {
        this.contacts = contacts;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_list_contact, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Contact contact = contacts.get(position);
        holder.mTvName.setText(contact.getName());

    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTvName;
        private ImageView mImgDelete, mImgEdit;
        private LinearLayout lnItem;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            mTvName = (TextView) itemView.findViewById(R.id.tvName);
            mImgDelete = (ImageView) itemView.findViewById(R.id.imgDelete);
            mImgEdit = (ImageView) itemView.findViewById(R.id.imgEdit);
            lnItem = (LinearLayout) itemView.findViewById(R.id.lnItem);
            mImgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Contact contact = contacts.get(getAdapterPosition());
                    context.diaglogDelete(contact.getName(), contact.getId());
                }
            });
            mImgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context.getApplication(), Update.class);
                    intent.putExtra(KEYUPDATE, contacts.get(getAdapterPosition()));
                    context.startActivity(intent);

                }
            });
            lnItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent myIntent = new Intent(context.getApplication(), Detail.class);
                    myIntent.putExtra(KEYDETAIL, contacts.get(getAdapterPosition()));
                    context.startActivity(myIntent);
                }
            });
        }
    }

}