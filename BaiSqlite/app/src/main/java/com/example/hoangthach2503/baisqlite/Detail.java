package com.example.hoangthach2503.baisqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class Detail extends AppCompatActivity {
    private TextView tvName, tvNumber, tvAddress, tvDate, tvTime, tvGendle ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        map();
        setView();
    }
    private void setView(){
        Intent mIntent=getIntent();
        Contact contact=(Contact)mIntent.getSerializableExtra(ContactAdapter.KEYDETAIL);
        tvName.setText(contact.getName());
        tvNumber.setText(contact.getNumber());
        tvGendle.setText(contact.getGendle());
        tvDate.setText(contact.getDate());
        tvTime.setText(contact.getTime());
        tvAddress.setText(contact.getAddress());

    }
    private void map(){
        tvDate= (TextView) findViewById(R.id.tvNgay);
        tvAddress= (TextView) findViewById(R.id.tvDiaChi);
        tvGendle= (TextView) findViewById(R.id.tvGioiTinh);
        tvTime= (TextView) findViewById(R.id.tvGio);
        tvName= (TextView) findViewById(R.id.tvTen);
        tvNumber= (TextView) findViewById(R.id.tvSo);
    }
}
