package hoangthach2503.docbaoturss;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class NewsDetailActivity extends AppCompatActivity {
private WebView wbData;
private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        wbData= (WebView) findViewById(R.id.wvData);
        Intent intent=getIntent();
        String url=intent.getStringExtra("LINK");
        if(url!=null){
            dialog=new ProgressDialog(this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();

            wbData.setWebViewClient(new WebViewClient(){

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    dialog.dismiss();
                }

                @Override
                public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                    dialog.dismiss();
                    super.onReceivedError(view, request, error);
                    Toast.makeText(NewsDetailActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                }
            });
            wbData.loadUrl(url);
        }
    }
}
