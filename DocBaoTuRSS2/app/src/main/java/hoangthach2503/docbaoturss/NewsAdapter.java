package hoangthach2503.docbaoturss;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;


/**
 * Created by Thach on 28/9/2017.
 */

public class NewsAdapter extends ArrayAdapter<NewsModel> {
    Context context;
    int resource;
    List<NewsModel> listCmt;

    public NewsAdapter(Context context, int resource, List<NewsModel> objects) {
        super(context, resource, objects);

        this.context = context;
        this.resource = resource;
        this.listCmt = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowItem = inflater.inflate(resource, parent, false);
        ImageView imageAvatar = (ImageView) rowItem.findViewById(R.id.imgAvatar);
        TextView tvTitle = (TextView) rowItem.findViewById(R.id.tvTitle);
        NewsModel md=getItem(position);
        tvTitle.setText(md.getTitle());
        Picasso.with(inflater.getContext()).load(md.getImageURL()).into(imageAvatar);
        return rowItem;
    }
}
