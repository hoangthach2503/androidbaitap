package hoangthach2503.docbaoturss;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private InputStreamReader inputStreamReader;
    private ListView lvNews;
    private NewsAdapter newsAdapter;
    private final String vnexpressRSS = "http://vietnamnet.vn/rss/home.rss";
    private final String sohaNews = "http://soha.vn/home.rss";
    private final String afamily = "http://afamily.vn/trang-chu.rss";
    private final String kenh14 = "http://dantri.com.vn/trangchu.rss";
    private final String genk = "http://gamek.vn/trang-chu.rss";
    private ArrayList<NewsModel> result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//-------------------------------------------------------------------------------------------------------
        lvNews = (ListView) findViewById(R.id.lvNews);
        lvNews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, NewsDetailActivity.class);
                intent.putExtra("LINK", newsAdapter.getItem(position).getLink());
                startActivity(intent);
            }
        });
        new LoadRSS().execute(vnexpressRSS);
      actionBarSetup("VnExpress");

    }

    class LoadRSS extends AsyncTask<String, Void, ArrayList<NewsModel>> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(ArrayList<NewsModel> newsModels) {
            dialog.dismiss();
            newsAdapter = new NewsAdapter(MainActivity.this, R.layout.news_item, result);
            lvNews.setAdapter(newsAdapter);
        }

        @Override
        protected ArrayList<NewsModel> doInBackground(String... strings) {
            String url = strings[0];
            result = new ArrayList<>();
            try {
                org.jsoup.nodes.Document doc = (org.jsoup.nodes.Document) Jsoup.connect(url).get();
                Elements elements = (Elements) doc.select("item");
                for (org.jsoup.nodes.Element item : elements) {
                    String title = item.select("title").text();
                    String link = item.select("link").text();
                    String description = item.select("description").text();
                    org.jsoup.nodes.Document docImage = Jsoup.parse(description);
                    String imageURL = docImage.select("img").get(0).attr("src");
                    result.add(new NewsModel(title, link, imageURL));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //---------------------------------------------------------------------------
    private void actionBarSetup(String tiltle) {
        android.support.v7.app.ActionBar ab = getSupportActionBar();
            ab.setTitle(tiltle);

    }
    //------------------------------------------------------------------------------

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add) {
            new LoadRSS().execute("https://muachung.vn/rss/rss-all.xml");
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();
            actionBarSetup("Thêm báo");
        } else if (id == R.id.addcm) {
            new LoadRSS().execute("https://muachung.vn/rss/rss-all.xml");
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();
            actionBarSetup("Thêm chuyên mục");
        } else if (id == R.id.edt) {
            new LoadRSS().execute("https://muachung.vn/rss/rss-all.xml");
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.size) {
            new LoadRSS().execute("https://muachung.vn/rss/rss-all.xml");
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();
            actionBarSetup("Chọn kích thước");
        }
        //-------------------------------
        else if (id == R.id.sohaPhim) {
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();
            actionBarSetup("Soha Phim");
        } else if (id == R.id.sohaNews) {
            new LoadRSS().execute(sohaNews);
            actionBarSetup("Soha News");

        } else if (id == R.id.afamily) {
            new LoadRSS().execute(afamily);
            actionBarSetup("Afamily");
        } else if (id == R.id.genk) {
            new LoadRSS().execute(genk);
            actionBarSetup("Gamek");
        } else if (id == R.id.muaChung) {
            new LoadRSS().execute("https://muachung.vn/rss/rss-all.xml");
            Toast.makeText(this, "Đang cập nhật mong bạn chờ đợi", Toast.LENGTH_SHORT).show();
            actionBarSetup("Mua Chung");
        } else if (id == R.id.kenh14) {
            new LoadRSS().execute(kenh14);
            actionBarSetup("Kenh14");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
