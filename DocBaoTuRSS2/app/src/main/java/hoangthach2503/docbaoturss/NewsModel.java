package hoangthach2503.docbaoturss;

/**
 * Created by Nguyen Hoang Thach on 11/5/2017.
 */

public class NewsModel {
    private String title;
    private String link;
    private String imageURL;

    public NewsModel(String title, String link, String imageURL) {
        this.title = title;
        this.link = link;
        this.imageURL = imageURL;
    }

    public NewsModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public String toString() {
        return "NewsModel{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", imageURL='" + imageURL + '\'' +
                '}';
    }
}
