package com.example.hoangthach2503.thoigianbieu;

/**
 * Created by Admin on 17/9/2017.
 */

public class CongViec {
    private String tenCV;
    private String NDCV;
private String thoiGian;

    public CongViec(String tenCV, String NDCV, String thoiGian) {
        this.tenCV = tenCV;
        this.NDCV = NDCV;
        this.thoiGian = thoiGian;
    }

    public String getTenCV() {
        return tenCV;
    }

    public void setTenCV(String tenCV) {
        this.tenCV = tenCV;
    }

    public String getNDCV() {
        return NDCV;
    }

    public void setNDCV(String NDCV) {
        this.NDCV = NDCV;
    }

    public String getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(String thoiGian) {
        this.thoiGian = thoiGian;
    }

    public CongViec() {
    }

    @Override
    public String toString() {
        return "Công Việc:   " + tenCV + "\n" +
                "Nội dung công việc:  " + NDCV + "\n" +
                "Thời gian:  " + thoiGian + "\n";
    }
}
