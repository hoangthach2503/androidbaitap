package com.example.hoangthach2503.thoigianbieu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NoiDungCongViec extends AppCompatActivity {
TextView txtNDCV;
    Button btnThoat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noi_dung_cong_viec);
        txtNDCV=(TextView) findViewById(R.id.txtNDCV);
        btnThoat=(Button) findViewById(R.id.btnThoat);
        btnThoat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Intent intent=getIntent();
       String nd= intent.getStringExtra("dulieu");
        txtNDCV.setText(nd);
    }
}
