package com.example.hoangthach2503.thoigianbieu;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    TextView txtDate, txtTime;
    EditText edtCV, edtND;
    ListView lvDSCV;
    Button btnThemCV;
    private ArrayList<String> cv;
    private ArrayList<CongViec> arrayCV;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvDSCV = (ListView) findViewById(R.id.lvDSCV);
        txtDate = (TextView) findViewById(R.id.txtDate);
        txtTime = (TextView) findViewById(R.id.txtTime);
        btnThemCV = (Button) findViewById(R.id.btnThemCV);
        edtCV = (EditText) findViewById(R.id.edtCV);
        edtND = (EditText) findViewById(R.id.edtND);
        Date time = new Date();
        SimpleDateFormat dinhDangTime = new SimpleDateFormat("HH:mm");
        String showTime = dinhDangTime.format(time.getTime());
        txtTime.setText(showTime);
        Date date = new Date();
        SimpleDateFormat dinhDangDate = new SimpleDateFormat("dd/MM/yyyy ");
        String showDate = dinhDangDate.format(date.getTime());
        txtDate.setText(showDate);
        cv = new ArrayList<>();
        arrayCV = new ArrayList<CongViec>();
        adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, cv);
        lvDSCV.setAdapter(adapter);
        btnThemCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tenCV = edtCV.getText().toString();
                String nd = edtND.getText().toString();
                String thoiGian = txtDate.getText().toString() + " - " + txtTime.getText().toString();
                CongViec congViec = new CongViec(tenCV, nd, thoiGian);
                cv.add(congViec.getTenCV().toString() + " - " + congViec.getThoiGian().toString());
                arrayCV.add(congViec);
                adapter.notifyDataSetChanged();
            }
        });
        lvDSCV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CongViec cviec = arrayCV.get(i);
                String nd=cviec.toString();
                Intent intent = new Intent(MainActivity.this, NoiDungCongViec.class);
                intent.putExtra("dulieu", nd);
                startActivity(intent);
            }
        });
        lvDSCV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                cv.remove(i);
                arrayCV.remove(i);
                adapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    public void showTimePickerDialog(View view) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int min = calendar.get(Calendar.MINUTE);
        TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                txtTime.setText(i + " : " + i1);
            }
        };
        TimePickerDialog dialog = new TimePickerDialog(this, onTimeSetListener, hour, min, true);
        dialog.show();
    }

    public void showDatePickerDialog(View view) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                txtDate.setText(i2 + "/" + (i1 + 1) + "/" + i);
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(this, onDateSetListener, year, month, day);
        dialog.show();
    }

}
