package com.example.hoangthach2503.baitapviewpage.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.hoangthach2503.baitapviewpage.Fragment1;
import com.example.hoangthach2503.baitapviewpage.Fragment2;
import com.example.hoangthach2503.baitapviewpage.Fragment3;
import com.example.hoangthach2503.baitapviewpage.MainActivity;

/**
 * Created by Admin on 1/10/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment1 tab1 = new Fragment1();
                return tab1;
            case 1:
                Fragment2 tab2 = new Fragment2();
                return tab2;
            case 2:
                Fragment3 tab3 = new Fragment3();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return mNumOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Văn hóa";
            case 1:
                return "Thể thao";
            case 2:
                return "Tin tức";
        }
        return null;
    }
}